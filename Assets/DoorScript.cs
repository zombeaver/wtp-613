﻿using UnityEngine;
using System.Collections;

        public class DoorScript : MonoBehaviour {
	   public Animator anim;
	   int openHash = Animator.StringToHash("Open");
	   int closeHash = Animator.StringToHash("Close");

	void Start (){
		//anim = GetComponent<Animator>();
	}
	void Update (){
	}
	void OnTriggerEnter (Collider other)
	{
		if(other.tag == "Player")
		{
			Debug.Log ("Player Enter");
			anim.SetBool("OpenDoor",true);
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.tag == "Player")
		{
			anim.SetBool("OpenDoor", false);
			Debug.Log ("Player Exit");
		}
	}
}
