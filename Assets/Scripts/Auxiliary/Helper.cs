﻿using UnityEngine;

public static class Helper
{
    public struct ClipPlaneVertices
    {
        public Vector3 upperLeft;
        public Vector3 upperRight;
        public Vector3 lowerLeft;
        public Vector3 lowerRight;
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        //remove full rotations from the angle passed in
        do
        {
            if (angle < -360)
            {
                angle += 360;
            }
            if (angle > 360)
            {
                angle -= 360;
            }
        }
        while (angle < -360 || angle > 360);

        return Mathf.Clamp(angle, min, max);
    }

    public static ClipPlaneVertices ClipPlaneAtNear(Vector3 pos)
    {
        var clipPlaneVertices = new ClipPlaneVertices();

        //Check that there is a camera in the scene
        if(Camera.main == null)
        {
            return clipPlaneVertices;
        }

        var camTrans = Camera.main.transform;
        var halfFOV = (Camera.main.fieldOfView / 2) * Mathf.Deg2Rad;
        var aspect = Camera.main.aspect;
        var dist = Camera.main.nearClipPlane;
        var height = dist * Mathf.Tan(halfFOV);
        var width = height * aspect;

        clipPlaneVertices.lowerRight = pos + camTrans.right * width;
        clipPlaneVertices.lowerRight -= camTrans.up * height;
        clipPlaneVertices.lowerRight += camTrans.forward * dist;

        clipPlaneVertices.lowerLeft = pos - camTrans.right * width;
        clipPlaneVertices.lowerLeft -= camTrans.up * height;
        clipPlaneVertices.lowerLeft += camTrans.forward * dist;

        clipPlaneVertices.upperRight = pos + camTrans.right * width;
        clipPlaneVertices.upperRight += camTrans.up * height;
        clipPlaneVertices.upperRight += camTrans.forward * dist;

        clipPlaneVertices.upperLeft = pos - camTrans.right * width;
        clipPlaneVertices.upperLeft += camTrans.up * height;
        clipPlaneVertices.upperLeft += camTrans.forward * dist;

        return clipPlaneVertices;
    }
}
