﻿using UnityEngine;
using System.Collections;

public class PlayerShooter : MonoBehaviour
{
	public int shotDamage = 10;
	public float fireDelay = 0.15f;
	public float range = 100f;

	float timer;
	Ray shootRay;
	RaycastHit shootHit;
	int shootableMask;
	LineRenderer gunLine;

	void Awake()
	{
		shootableMask = LayerMask.GetMask("Shootable");
		gunLine = GetComponent<LineRenderer>();
	}

	void Update()
	{
		timer += Time.deltaTime;

		if (Input.GetButton ("Fire1") && timer >= fireDelay)
		{
			Shoot();
		}

        if (timer >= 0.05)
		{
			DisableEffects();
		}  
    }

	public void DisableEffects()
	{
		gunLine.enabled = false;
	}

	void Shoot()
	{
		timer = 0f;

		gunLine.enabled = true;
		gunLine.SetPosition (0, transform.position);

        shootRay = Camera.main.ScreenPointToRay(new Vector3(Screen.width/2, Screen.height/2, 0));
        //shootRay = Camera.main.ScreenPointToRay (Input.mousePosition);
        ////shootRay.origin = transform.position;
        ////shootRay.direction = Camera.main.ScreenToWorldPoint (Input.mousePosition);

        if (Physics.Raycast (shootRay, out shootHit, range, shootableMask)) {
			EnemyHealth enemyhealth = shootHit.collider.GetComponent<EnemyHealth> ();
			if (enemyhealth != null)
			{
				enemyhealth.TakeDamage (shotDamage, shootHit.point);
			}
			gunLine.SetPosition (1, shootHit.point);
		}
		else
		{
			gunLine.SetPosition(1, shootRay.origin + shootRay.direction * range);
		}
	}
}
