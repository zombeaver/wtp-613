﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour {

    public float startHealth = 100;
    public float health;
	
    void Start()
    {
        health = startHealth;
    }

	void Update()
    {
	
	}

    void OnGUI()
    {
        GUI.HorizontalSlider(new Rect(25, 25, 100, 30), health, 0.0f, startHealth);
    }

    public bool TakeDamage(float damage)
    {
        if (health > 0)
        {
            health -= damage;
            return true;
        }
        else
        {
            return false;
        }
    }
}
