﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;

    public Transform targetLookAt;

    public float dist = 5f;
    public float distMin = 3f;
    public float distMax = 10f;
    public float distSmooth = 0.05f;
    public float distResumeSmooth = 1f;
    public float x_MouseSensitivity = 5f;
    public float y_MouseSensitivity = 5f;
    public float mouseWheelSensitivity = 5f;
    public float x_Smooth = 0.05f;
    public float y_Smooth = 0.1f;
    public float y_LimitMin = -40f;
    public float y_LimitMax = 80f;
    public float occlusionDistStep = 0.5f;
    public int maxOcclusionChecks = 10;

    private float minOccludedDist = 0.25f;
    private float mouseX = 0f;
    private float mouseY = 0f;
    private float velX = 0f;
    private float velY = 0f;
    private float velZ = 0f;
    private float velDist = 0f;
    private float startDist = 0f;
    private float desiredDist = 0f;
    private Vector3 desiredPosition = Vector3.zero;
    private Vector3 position = Vector3.zero;
    private float distSmoothOccl = 0f;
    private float preOccludedDist = 0f;

    void Awake()
    {
        instance = this;
    }	

	void Start()
    {
        //Verify that distance is within bounds
        dist = Mathf.Clamp(dist, distMin, distMax);
        startDist = dist;
        Reset();
	}
	
	void LateUpdate()
    {
        //if targetLookAt doesn't exist, exit
        if (targetLookAt == null)
        {
            return;
        }

        //take in player inputs
        HandlePlayerIn();

        var count = 0;
        do
        {
            //calculate the intended position for the camera to move to
            CalculateDesiredPosition();
            count++;
        }
        while (CheckIfOccluded(count));

        //move the camera with smotthing applied
        UpdatePosition();
	}

    void HandlePlayerIn()
    {
        //deadzone for mousewheel, for wheels that don't have click-based scroll thingy
        var deadZone = 0.01f;

        mouseX += Input.GetAxis("Mouse X") * x_MouseSensitivity;
        mouseY -= Input.GetAxis("Mouse Y") * y_MouseSensitivity;

        //limit the mouse y rotation so it doesn't flip around the character
        mouseY = Helper.ClampAngle(mouseY, y_LimitMin, y_LimitMax);

        //make sure mouse wheel is actually scrolled
        if (Input.GetAxis("Mouse ScrollWheel") < -deadZone || Input.GetAxis("Mouse ScrollWheel") > deadZone)
        {
            //store the desired amout of scroll in desiredDistance, within the limits
            desiredDist = Mathf.Clamp(dist - Input.GetAxis("Mouse ScrollWheel") * mouseWheelSensitivity, distMin, distMax);

            preOccludedDist = desiredDist;
            distSmoothOccl = distSmooth;
        }
    }

    void CalculateDesiredPosition()
    {
        //evaluate the distance, with smoothing applied
        ResetDesiredDistance();
        dist = Mathf.SmoothDamp(dist, desiredDist, ref velDist, distSmoothOccl);

        //calculate desiredPosition, need to pass mouse inputs in reverse since it uses rotation about an axis
        desiredPosition = CalculatePosition(mouseY, mouseX, dist);
    }

    Vector3 CalculatePosition(float rotX, float rotY, float distance)
    {
        //distance is negative so it's pointing behind the character not in front
        Vector3 direction = new Vector3(0, 0, -distance);
        Quaternion rotation = Quaternion.Euler(rotX, rotY, 0);

        return targetLookAt.position + rotation * direction;
    }

    bool CheckIfOccluded(int count)
    {
        bool isOccluded = false;

        var nearestDist = CheckCamPoints(targetLookAt.position, desiredPosition);

        if(nearestDist != -1)
        {
            if(count < maxOcclusionChecks)
            {
                isOccluded = true;
                dist -= occlusionDistStep;

                if(dist < minOccludedDist)
                {
                    dist = minOccludedDist;
                }
            }
            else
            {
                dist = nearestDist - Camera.main.nearClipPlane;
            }

            desiredDist = dist;
            distSmoothOccl = distResumeSmooth;
        }

        return isOccluded;
    }

    float CheckCamPoints(Vector3 from, Vector3 to)
    {
        var nearestDist = -1f;

        RaycastHit hitInfo;

        Helper.ClipPlaneVertices clipPlanePoints = Helper.ClipPlaneAtNear(to);

        //Draw lines in the scene view to visualize
        Debug.DrawLine(from, to + transform.forward * -Camera.main.nearClipPlane, Color.red);
        Debug.DrawLine(from, clipPlanePoints.upperLeft);
        Debug.DrawLine(from, clipPlanePoints.lowerLeft);
        Debug.DrawLine(from, clipPlanePoints.upperRight);
        Debug.DrawLine(from, clipPlanePoints.lowerRight);

        Debug.DrawLine(clipPlanePoints.upperLeft, clipPlanePoints.upperRight);
        Debug.DrawLine(clipPlanePoints.upperRight, clipPlanePoints.lowerRight);
        Debug.DrawLine(clipPlanePoints.lowerRight, clipPlanePoints.lowerLeft);
        Debug.DrawLine(clipPlanePoints.lowerLeft, clipPlanePoints.upperLeft);

        if(Physics.Linecast(from, clipPlanePoints.upperLeft, out hitInfo) && hitInfo.collider.tag != "Player")
        {
            nearestDist = hitInfo.distance;
        }

        if (Physics.Linecast(from, clipPlanePoints.lowerLeft, out hitInfo) && hitInfo.collider.tag != "Player")
        {
            if (hitInfo.distance < nearestDist || nearestDist == -1)
            {
                nearestDist = hitInfo.distance;
            }
        }

        if (Physics.Linecast(from, clipPlanePoints.upperRight, out hitInfo) && hitInfo.collider.tag != "Player")
        {
            if (hitInfo.distance < nearestDist || nearestDist == -1)
            {
                nearestDist = hitInfo.distance;
            }
        }

        if (Physics.Linecast(from, clipPlanePoints.lowerRight, out hitInfo) && hitInfo.collider.tag != "Player")
        {
            if (hitInfo.distance < nearestDist || nearestDist == -1)
            {
                nearestDist = hitInfo.distance;
            }
        }

        if (Physics.Linecast(from, to + transform.forward * -Camera.main.nearClipPlane, out hitInfo) && hitInfo.collider.tag != "Player")
        {
            if (hitInfo.distance < nearestDist || nearestDist == -1)
            {
                nearestDist = hitInfo.distance;
            }
        }

        return nearestDist;
    }

    void ResetDesiredDistance()
    {
        if(desiredDist < preOccludedDist)
        {
            var pos = CalculatePosition(mouseY, mouseX, preOccludedDist);

            var nearestDist = CheckCamPoints(targetLookAt.position, pos);

            if(nearestDist == -1 || nearestDist > preOccludedDist)
            {
                desiredDist = preOccludedDist;
            }
        }
    }

    void UpdatePosition()
    {
        //smooth each aspect seperately, allows for individual tweaking later on
        //x and y use the same smoothing since they're both controlling the orbital motion
        var posX = Mathf.SmoothDamp(position.x, desiredPosition.x, ref velX, x_Smooth);
        var posY = Mathf.SmoothDamp(position.y, desiredPosition.y, ref velY, y_Smooth);
        var posZ = Mathf.SmoothDamp(position.z, desiredPosition.z, ref velZ, x_Smooth);
        position = new Vector3(posX, posY, posZ);

        //set the camera's position to be equal to the calculated position
        transform.position = position;

        //keep the camera looking at the target
        transform.LookAt(targetLookAt);
    }

    public void Reset()
    {
        mouseX = 0;
        mouseY = 10;
        dist = startDist;
        desiredDist = dist;
        preOccludedDist = dist;
    }

    public static void UseOrCreateMainCamera()
    {
        GameObject tempCamera;
        GameObject targetLookAtLocal;
        CameraController myCamera;

        //assign the camera to tempCamera or make one if it's missing somehow
        if (Camera.main != null)
        {
            tempCamera = Camera.main.gameObject;
        }
        else
        {
            tempCamera = new GameObject("Main Camera");
            tempCamera.AddComponent<Camera>();
            tempCamera.tag = "MainCamera";
        }

        //add the script to the camera
        tempCamera.AddComponent<CameraController>();
        myCamera = tempCamera.GetComponent("CameraController") as CameraController;

        targetLookAtLocal = GameObject.Find("TargetLookAt") as GameObject;

        if (targetLookAtLocal == null)
        {
            targetLookAtLocal = new GameObject("TargetLookAt");
            targetLookAtLocal.transform.position = Vector3.zero;
        }

        myCamera.targetLookAt = targetLookAtLocal.transform;
    }
}
