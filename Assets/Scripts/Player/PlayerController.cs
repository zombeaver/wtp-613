﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public static CharacterController characterController;
    public static PlayerController instance;

    //debug for hurting self
    public static PlayerHealth2 playerHealth;

    private float gravityFactor = 1f;

	void Awake()
    {
        //take in reference to character controller
        characterController = GetComponent("CharacterController") as CharacterController;

        //reference to self
        instance = this;

        //call the camera setup
        CameraController.UseOrCreateMainCamera();

        //debug for hurting self
        playerHealth = GetComponent<PlayerHealth2>();
	}
	
	void Update()
    {
        //debug for hurting self
        if (Input.GetKey(KeyCode.H))
        {
            playerHealth.TakeDamage(5);
			Debug.Log ("Player DamageH");
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            Application.LoadLevel(0);
        }
        
        //if the camera vanishes for some reason, stop doing everthing
        if (Camera.main == null)
        {
            return;
        }

        GetMoveIn();
        
        PlayerMotor.instance.UpdateMotor();
	}

    void GetMoveIn()
    {
        //create a deadzone so that minute returns aren't processed, resulting in a sliding character
        var deadZone = 0.1f;
        
        //clear moveVector each time so it doesn't become additive
        PlayerMotor.instance.moveVector = Vector3.zero;

        if (Input.GetAxis("Vertical") > deadZone || Input.GetAxis("Vertical") < -deadZone)
        {
            PlayerMotor.instance.moveVector += new Vector3(0, 0, Input.GetAxis("Vertical"));
        }

        if (Input.GetAxis("Horizontal") > deadZone || Input.GetAxis("Horizontal") < -deadZone)
        {
            PlayerMotor.instance.moveVector += new Vector3(Input.GetAxis("Horizontal"), 0, 0);
        }

        if (!characterController.isGrounded)
        {
            PlayerMotor.instance.moveVector += new Vector3(0, -gravityFactor, 0);
        }
    }
}
