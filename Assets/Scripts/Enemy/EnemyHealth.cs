using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public float sinkSpeed = 2.5f;
	public AudioClip deathClip;
	
	AudioSource enemyAudio;
    BoxCollider boxCollider;
    public bool isDead;
    //bool isSinking;


	void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
        //enemyAudio.clip = deathClip;

        currentHealth = startingHealth;
	}
	
	void Update()
    {
	    /*if(isSinking)
        {
            transform.Translate(-Vector3.up * sinkSpeed * Time.deltaTime);
        }*/
	}

    public void TakeDamage(int amount, Vector3 hitPoint)
    {
        if(isDead)
        {
            return;
        }

        currentHealth -= amount;

        if(currentHealth <= 0)
        {
            Death();
        }
    }

    void Death()
    {
        isDead = true;

        boxCollider.enabled = false;

		//deathClip sound effect should play and stop the hurt sound 
		enemyAudio.Play ();

        StartSinking();
    }

    public void StartSinking()
    {
        GetComponent<NavMeshAgent>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        //isSinking = true;
        //Destroy(gameObject, 2f);
    }
}
