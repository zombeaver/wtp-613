﻿using UnityEngine;
using System.Collections;

public class PlayerMotor : MonoBehaviour
{
    public static PlayerMotor instance;

    public float moveSpeed = 10f;

    public Vector3 moveVector {get; set;}

	void Awake()
    {
        instance = this;
	}
	
	//don't use normal update, so motor only updates when called in controller
	public void UpdateMotor()
    {
        SnapCharToCamera();
        ProcessMotion();
	}

    void ProcessMotion()
    {
        //translate movement into worldspace
        moveVector = transform.TransformDirection(moveVector);

        //normalize the vector if it's over 1, such as when doing diagonals
        if (moveVector.magnitude > 1)
        {
            moveVector = Vector3.Normalize(moveVector);
        }

        //speed up the movement
        moveVector *= moveSpeed;

        //make it units/sec instead of units/frame
        moveVector *= Time.deltaTime;

        //actually move the character
        PlayerController.characterController.Move(moveVector);
    }

    void SnapCharToCamera()
    {
        //only snap to camera if moving
        //if (moveVector.z != 0 || moveVector.x != 0)
        //{
            transform.rotation = Quaternion.Euler(transform.eulerAngles.x,
                                                  Camera.main.transform.eulerAngles.y,
                                                  transform.eulerAngles.z);
        //}
    }
}
