﻿using UnityEngine;
using System.Collections;


public class EnemyController : MonoBehaviour
{
	Transform player;               // Reference to the player's position.
	NavMeshAgent nav;               // Reference to the nav mesh agent.
    Animator anim;
    EnemyHealth health;
    PlayerHealth2 playerHealth;

    public float aggroDist = 10;
    Vector3 distFromPlayer;
    bool idling = true;
    bool attacking = false;
    float timer = 0.0f;
    public int damageDealt = 10;
    bool hasAttacked = false;
	public AudioSource enemyhurtClip;
	public AudioSource enemyattackClip;
	public AudioSource enemydeathClip;
	AudioSource playerAudio; 

	void Awake ()
	{
		// Set up the references.
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		nav = GetComponent <NavMeshAgent> ();
		playerAudio = GetComponent <AudioSource> ();
		health = GetComponent <EnemyHealth> ();
        anim = GetComponent <Animator> ();
		//playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent <PlayerHealth2> ();
	}

		
	void Update ()
	{
        timer += Time.deltaTime;

        nav.destination = player.position;

        distFromPlayer = transform.position - player.position;
        if (distFromPlayer.magnitude <= aggroDist)
        {
            idling = false;
			nav.Resume ();
        }
        else
        {
            idling = true;
            nav.Stop ();
        }

		if (health.isDead)
		{
            anim.SetBool("Death", true);
            nav.Stop ();

			//playerAudio.clip = enemydeathClip;
			//playerAudio.Play ();

		}

        if(idling)
        {
            anim.SetBool("IsIdle", true);
        }
        else if(attacking)
        {
            ;
        }
        else
        {
            anim.SetBool("IsIdle", false);
        }
	}

    void OnTriggerEnter(Collider col)
    {
		if (col.tag == "Player")
        {
			timer = 0.0f;
			anim.SetTrigger ("AttackTrigger");
			hasAttacked = false;
			Debug.Log ("Has Attaked");

			//playerAudio.clip = enemyattackClip;
			//playerAudio.Play ();

		}
    }

    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Player")
        {
            playerHealth = col.GetComponent<PlayerHealth2>();
            if(!playerHealth)
            {
                debug.log("couldn't find component");
            }

            if (timer > 1.15 && !hasAttacked)
            {
				playerHealth.TakeDamage (damageDealt);
				Debug.Log ("Player Taken Damage");
				hasAttacked = true;
			}
		}
    }
}
