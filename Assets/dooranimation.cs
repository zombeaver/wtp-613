﻿using UnityEngine;
using System.Collections;

public class DoorAnimation : MonoBehaviour
{
	public bool requirePipe;                     // Whether or not a Pipe is required.
	public AudioClip doorSwishClip;             // Clip to play when the doors open or close.
	public AudioClip accessDeniedClip;          // Clip to play when the player doesn't have the key for the door.
	
	
	private Animator anim;                      // Reference to the animator component.
	private GameObject player;                  // Reference to the player GameObject.
	//private PlayerInventory playerInventory;    // Reference to the PlayerInventory script.
	private int count;                          // The number of colliders present that should open the doors.
	
	
	void Awake ()
	{
		// Setting up the references.
		anim = GetComponent<Animator>();
		player = GameObject.FindGameObjectWithTag("Player");
		//playerInventory = player.GetComponent<PlayerInventory>();
	}
	
	void OnTriggerEnter (Collider other)
	{
        // If the triggering gameobject is the player...
        if (other.gameObject == player)
        {
            // ... if this door requires a pipe...
            if (requirePipe)
            {
                // ... if the player has the pipe...
                /*if (playerInventory.hasPipe)
                {
                    // ... increase the count of triggering objects.
                    count++;
                }
                else
                {
                    // If the player doesn't have the pipe play the access denied audio clip.
                    GetComponent<AudioSource>().clip = accessDeniedClip;
                    GetComponent<AudioSource>().Play();
                }*/
            }
            else
            {
                // If the door doesn't require a pipe, increase the count of triggering objects.
                count++;
            }
		}
	}
	
	void OnTriggerExit (Collider other)
	{
        // If the leaving gameobject is the player 
        if (other.gameObject == player)
        {
            // decrease the count of triggering objects.
            count = Mathf.Max(0, count - 1);
        }
	}
	
	void Update ()
	{
		// Set the open parameter.
		//anim.SetBool(hash.openBool,count > 0);
		
		// If the door is opening or closing...
		if(anim.IsInTransition(0) && !GetComponent<AudioSource>().isPlaying)
		{
			// ... play the door swish audio clip.
			GetComponent<AudioSource>().clip = doorSwishClip;
			GetComponent<AudioSource>().Play();
		}
	}
}